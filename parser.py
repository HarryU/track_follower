#!/usr/bin/python3
from bokeh.plotting import figure, save, show, output_file, ColumnDataSource
from bokeh.layouts import row, column
import bokeh.models.widgets.tables as Tables
from bokeh.models import CustomJS, Spacer, Div, HoverTool, TapTool
from bokeh.tile_providers import STAMEN_TERRAIN as tile_provider
import urllib
import math
import pandas
import re
import http
import pickle
import hashlib
import os
import time
import json
import collections
import datetime

DEBUG = False

RACENAME = 'transam19'

UPDATE_TIME = 60 * 5
if DEBUG:
    output_type = show
    UPDATE_TIME = 60 * 60 * 24
else:
    output_type = save

javascript_tuple = collections.namedtuple('javascript', ('track', 'overview'))


colours = collections.defaultdict(lambda : 'brown')
colours.update(bekki='red', harry='blue', jack='green', modge='magenta', rich='orange', tristan='cyan', heroes_of_the_road='gray')


def latlonToWM(lon, lat):
    x = lon * 20037508.34 / 180
    y = math.log(math.tan((90 + lat) * math.pi / 360)) / (math.pi / 180)
    y = y * 20037508.34 / 180
    return x, y


def get_avatar(name):
    url = "http://trackleaders.com/spot/"+RACENAME+"/avatars/" + name + ".jpg"
    p = figure(x_range=(0, 255), y_range=(0, 255))
    p.image_url(url=[url], x=0, y=0, w=255, h=255)
    return p


def draw_plot(dataframe, checkpoints):
    to_draw = []
    for i, row in dataframe.iterrows():
        to_draw.append((row['name'], row['distance'], row['colour'], row['status'], row['race_status']))

    sorted_draw = sorted(to_draw, key=lambda x: x[1], reverse=False)
    rider_names = [item[0] for item in sorted_draw]
    dot = figure(title="Leaderboard", tools="hover", y_range=rider_names, x_range=[checkpoints[-1], 0])
    dot.background_fill_color = "beige"
    for checkpoint in checkpoints:
        dot.segment(checkpoints[-1]-checkpoint, 0, checkpoints[-1]-checkpoint, len(sorted_draw), line_color='black', line_width=1)

    for idx, (name, distance, colour, rider_status, race_status) in enumerate(sorted_draw):
        rider = idx + 0.5
        circle_kws = dict(size=5, fill_color=colour, line_color=colour, line_width=3)

        if race_status == "Active":
            line_width = 2
            if rider_status == "Pro":
                draw_style = dot.diamond
            else:
                draw_style = dot.circle
            style = 'solid'
        elif race_status == "Finished":
            line_width = 5
            if rider_status == "Pro":
                draw_style = dot.diamond
            else:
                draw_style = dot.circle
            style = 'solid'
        else:
            line_width = 2
            draw_style = dot.x
            del circle_kws['fill_color']
            del circle_kws['line_width']
            circle_kws['size'] = 7
            style = "dashed"

        draw_style(checkpoints[-1] - distance, rider, **circle_kws)

        dot.segment(checkpoints[-1], rider, checkpoints[-1] - distance, rider, line_width=line_width, line_dash=style,
                    line_color=colour)

    return dot


def get_route_mile(text):
    if text is None:
        return 0
    else:
        if RACENAME.startswith("transam"):
            look_for = "Route mile"
            distance_units='mi'
        else:
            look_for = "Straight Tracking Distance covered"
            distance_units='km'
        groups = re.findall("<tr><td>{}</td><td>([0-9]*.[0-9]*) {}".format(look_for, distance_units), text)
        if len(groups):
            return float(groups[-1])
        else:
            return 0


def get_race_status(text):
    if text is None:
        return "Not Available"
    else:
        groups = re.findall("<tr><td>Race Status<\/td><td>([A-z]*)<", text)
        return groups[-1]


def get_file_name_for_url(url):
    return os.path.join("cache", str(int.from_bytes(hashlib.md5(url.encode()).digest(), byteorder='little')))


def get_file_from_web(url):
    if not os.path.exists("cache"):
        os.mkdir("cache")
    print(url)
    try:
        mtime = os.path.getmtime(get_file_name_for_url(url))
        if (time.time() - mtime) < UPDATE_TIME:  # if modified time is less than 5 minutes
            print("using cache")
            with open(get_file_name_for_url(url), 'rb') as filename:
                return pickle.load(filename)
    except FileNotFoundError:
        pass

    try:
        data = urllib.request.urlopen(url).read().decode()
    except urllib.error.HTTPError:
        print("not found")
        return None
    except http.client.IncompleteRead:
        print("incomplete read, using cache")
        try:
            with open(get_file_name_for_url(url), 'rb') as filename:
                return pickle.load(filename)
        except FileNotFoundError:
            print("no cache found")
            return None
    with open(get_file_name_for_url(url), 'wb') as filename:
        pickle.dump(data, filename)
    return data


def get_checkpoint_information():
    checkpoint_file = get_file_from_web('http://trackleaders.com/spot/'+RACENAME+'/checkgen.js')
    groups = re.findall('L\.marker\(\[(-?[0-9]*.[0-9]*),(-?[0-9]*.[0-9])*\].*"<b>(\w+)<\/b>', checkpoint_file)
    ret = {}
    for group in groups:
        check_position = dict()
        y, x = latlonToWM(float(group[1]), float(group[0]))
        check_position['lat'] = x
        check_position['lon'] = y
        check_position['name'] = group[2]
        check_position['Route mile'] = 0
        ret[group[2]] = check_position

    if RACENAME.startswith("transam"):
        groups = re.findall('(?<=<b>)([A-z! ,-]*).*<br>Route mile: ([0-9]*.[0-9]*)', checkpoint_file)
    else:
        groups = [('CP1', 850), ('CP2', 1300),('CP3', 2060),('CP4', 3120),('FINISH',3800)]
    for group in groups:
        ret[group[0]]['Route mile'] = float(group[1])
    return pandas.DataFrame.from_records(list(ret.values())).sort_values('Route mile')


def get_overview(name):
    return get_file_from_web(get_overview_url(name))


def get_overview_url(name):
    return "http://trackleaders.com/"+RACENAME+"i.php?name=" + name.replace(" ", "_")


def get_track(name):
    url = "http://trackleaders.com/spot/"+RACENAME+"/" + name.replace(" ", "_") + ".js"
    return get_file_from_web(url)


def get_mobile_summary():
    url = "http://trackleaders.com/spot/"+RACENAME+"/summary.php"
    return get_file_from_web(url)


def get_offroute_riders():
    text = get_mobile_summary()
    groups = re.findall(r"<tr><td>([A-z ]+).*\n<td>[0-9]*.[0-9]* mph</td><td>Off Route</td>", text)
    return [('off_course', name, 'unknown') for name in groups]


def extract_track_data(name, data):
    xs = []
    ys = []
    names=[]

    if data is not None:
        polypaths = data.split("polypath.push")[1:]
        latlon = [[float(x), float(y)] for x, y in [p.split("LatLng(")[1].split(")")[0].split(",") for p in polypaths]]

        for x, y in latlon:
            y, x = latlonToWM(y, x)
            xs.append(x)
            ys.append(y)
            names.append(name)

    return ColumnDataSource(data=dict(lat=xs, lon=ys, name=names))


def plot_person_on_map(b_fig, track, colour, race_number, linewidth=2):
    if len(track.data['lon'])>0:
        b_fig.circle('lon', 'lat', source=track, color=colour, line_width=linewidth)
        b_fig.text(track.data['lon'][-1], track.data['lat'][-1], text=dict(value=str(race_number)))

def plot_people_on_map(map_plot, frame, track_data):
    for _, member in frame.iterrows():
        plot_person_on_map(map_plot, track_data[member['name']], member['colour'],
                       member['bib'], linewidth=0.25)

    map_plot.add_tools(HoverTool(tooltips=[("name", "@name")]))


def plot_checkpoints_on_map(map, info):
    map.circle('lon', 'lat', source=info, color='black', line_width=2, size=8, fill_alpha=0)
    map_plot.add_tools(HoverTool(tooltips=[("name", "@name")]))


def get_bib_numbers():
    numbers = collections.defaultdict(lambda:-1)
    with open(RACENAME+".csv") as file:
        lines = file.readlines()
        for line in lines:
            items = line.split(",")
            numbers[items[0]] = int(items[1])

    return numbers


def get_furthest_active_rider(frame):
    return frame[frame['race_status'] == "Active"].distance.idxmax()

check_info = get_checkpoint_information()
checkpoint_distances = sorted(check_info['Route mile'])

output_file("public/"+RACENAME+".html")


def get_news(frame):
    names = [name.replace(" ", "_") for name in frame.name]
    news=[]
    text = get_file_from_web('http://trackleaders.com/news?prefix='+RACENAME)
    for line in text.split("\n"):
        groups = re.findall("<li><b><a.*name=([A-z]*)&.*", line)
        for group in groups:
            if group in names:
                line = line.replace("//","http://")
                line = line.replace(r'href="/',r'href="http://trackleaders.com/')
                line = line.replace("height=25", "height=50")
                news.append(line)
    return news

# get all off route riders that are not already in the list of riders
#names=[name for _,name,_ in teams ]
#teams.extend([rider for rider in get_offroute_riders() if rider[1] not in names])

frame = pandas.read_csv(RACENAME+".csv")

javascript_files = {name: javascript_tuple(get_track(name), get_overview(name)) for name in frame.name}

news_items = get_news(frame)

frame['distance'] = frame.apply(lambda row: get_route_mile(javascript_files[row['name']].overview), axis=1)
frame['race_status'] = frame.apply(lambda row: get_race_status(javascript_files[row['name']].overview), axis=1)
frame['colour'] = frame.apply(lambda row: colours[row.team], axis=1)
frame['remaining'] = checkpoint_distances[-1] - frame.distance
frame['url'] = frame.apply(lambda row: get_overview_url(row['name']), axis=1)


# find all scratched and off-course riders and remove them from the dataframe
scratched=frame[frame['race_status']=="Scratched"]
scratched_and_oc = scratched[scratched['team']=='off_course']
frame = frame.drop(scratched_and_oc.index, axis=0)


table_columns = [Tables.TableColumn(field=name, title=name) for name in frame.columns]
columnSource = ColumnDataSource(data=frame)
table = Tables.DataTable(source=columnSource, columns=table_columns, selectable=True)

dots = draw_plot(frame, checkpoint_distances)

# js for opening new window when clicking on table
tableSelectedCallback = CustomJS(args=dict(source=columnSource, table=json.loads(frame.to_json(orient='index'))),code="""
    window.open(table[String(source.selected.indices)].url);
""")
columnSource.js_on_change('selected',tableSelectedCallback)

# range bounds supplied in web mercator coordinates
map_plot = figure(x_axis_type="mercator", y_axis_type="mercator")
map_plot.add_tile(tile_provider)

map_plot_interest = figure(x_axis_type="mercator", y_axis_type="mercator")
map_plot_interest.add_tile(tile_provider)

track_data = {member['name']: extract_track_data(member['name'], javascript_files[member['name']].track) for _, member in frame.iterrows()}

#frame_of_interest = frame[frame['name'].isin(['Gunnar Ohlanders', 'Edward Tapp'])]


plot_people_on_map(map_plot, frame, track_data)
#plot_people_on_map(map_plot_interest, frame_of_interest, track_data)
plot_checkpoints_on_map(map_plot, check_info)

footer = Div(text="Last updated: {}".format(datetime.datetime.now()))

news_div = Div(text='<ul id="tlNews2">'+''.join(news_items[:5])+'</ul>')

output_type(column(row(map_plot, dots), row(map_plot_interest, news_div), row(table, footer)))

